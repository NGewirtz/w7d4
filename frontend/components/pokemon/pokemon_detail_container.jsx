import {connect} from 'react-redux';
import PokemonDetail from './pokemon_detail';
import { requestSinglePokemon, receiveSinglePokemon } from '../../actions/pokemon_actions';
import React from 'react';

const mapStateToProps = (state) => {
  return { pokemon: state.entities.pokemon, items: state.entities.items };
};

const mapDispatchToProps = (dispatch) => {
  return {
    requestSinglePokemon: (id) => {
        return dispatch(requestSinglePokemon(id));
      }
    };
  };



export default connect(mapStateToProps, mapDispatchToProps)(PokemonDetail);
