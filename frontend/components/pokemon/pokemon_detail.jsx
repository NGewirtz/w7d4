import React from 'react';

class PokemonDetail extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.requestSinglePokemon(this.props.match.params.pokemonId);
  }

  componentWillReceiveProps(newProps) {
    const newId = newProps.match.params.pokemonId;
    const oldId = this.props.match.params.pokemonId;

    if (newId !== oldId) {
      this.props.requestSinglePokemon(newId);
    }
  }

  render () {
    const id = parseInt(this.props.match.params.pokemonId);
    const poke = this.props.pokemon[id] || { name: '', attack: "", defense: "", type: ''};
    let items = this.props.items || [];
    items.map((item) => {
      return (
        <li>{item.name}</li>
      );
    });
    return (
      <div className = 'detail'>
        <h1>Name: {poke.name}</h1>
        <img src= {poke.image_url}/>
        <h4>Type: {poke.poke_type}</h4>
        <h4>Attack: {poke.attack}</h4>
        <h4>Defense: {poke.defense}</h4>
        <h4>Moves: {poke.moves}</h4>
        <ul>
          {items}
        </ul>
      </div>
    );
  }
}


export default PokemonDetail;
