import React from 'react';
import { Link, Route } from 'react-router-dom';
import PokemonDetailContainer from './pokemon_detail_container';

const PokemonIndexItem = ({name, image_url, id}) => {
  return (
    <div>
      <Link to = {`/pokemon/${id}`} >
        <li>
          {name}
          <img src = {image_url} />
        </li>
      </Link>
    </div>
  )
}

export default PokemonIndexItem
