import React from 'react';
import PokemonIndexItem from './pokemon_index_item';
import { Link, Route } from 'react-router-dom';
import PokemonDetailContainer from './pokemon_detail_container';

class PokemonIndex extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.requestAllPokemon();
  }

  render() {
    const allPokemon = this.props.pokemon.map((poke) => {
      return (
        <PokemonIndexItem name = {poke.name} image_url = {poke.image_url} key={poke.id} id = {poke.id}/>
      );
    });
    return (
      <div className = "flex">
        <ul>
          {allPokemon}
        </ul>
        <Route path ="/pokemon/:pokemonId" component = {PokemonDetailContainer}></Route>
      </div>
  );
  }
}

export default PokemonIndex;
