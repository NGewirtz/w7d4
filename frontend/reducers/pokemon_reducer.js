import { RECEIVE_ALL_POKEMON, RECEIVE_SINGLE_POKEMON } from '../actions/pokemon_actions';

const pokemonReducer = (state = {}, action) => {
  switch(action.type) {
    case RECEIVE_ALL_POKEMON:
      let newState = Object.assign({}, state, action.pokemon);
      return newState;
    case RECEIVE_SINGLE_POKEMON:

    // console.log(action);
      return Object.assign({}, state, { [action.pokemon.id]: action.pokemon} );
    default:
      return state;
  }
};

export default pokemonReducer;
