import { RECEIVE_SINGLE_POKEMON } from '../actions/pokemon_actions';

const uiReducer = (state = {}, action) => {
  switch(action.type) {
    case RECEIVE_SINGLE_POKEMON:
      return Object.assign({}, state, { pokeDisplay: action.pokemon.id });
    default:
      return state;
  }
};

export default uiReducer;
