import React from 'react';
import ReactDOM from 'react-dom';
import * as APIUtil from './util/api_util';
import {receiveAllPokemon, requestSinglePokemon, requestAllPokemon} from './actions/pokemon_actions';
import configureStore from './store/store';
import { selectAllPokemon } from './reducers/selectors';
import Root from './components/root';
import { HashRouter, Route } from 'react-router-dom';


document.addEventListener('DOMContentLoaded', () => {
  window.selectAllPokemon = selectAllPokemon;
  window.requestSinglePokemon = requestSinglePokemon;
  window.requestAllPokemon = requestAllPokemon;
  const store = configureStore();
  window.store = store;
  window.fetchAllPokemon = APIUtil.fetchAllPokemon;
  window.receiveAllPokemon = receiveAllPokemon;
  const root = document.getElementById('root');
  ReactDOM.render(<Root store={store}/>, root);
});
