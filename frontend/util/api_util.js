export const fetchAllPokemon = () => {
  return $.ajax('api/pokemon');
};

export const fetchSinglePokemon = (id) => {
  console.log(id);
  return $.ajax(`/api/pokemon/${id}`);
};
